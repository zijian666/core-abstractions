# [zijian666.Core.Abstractions](https://gitee.com/zijian666/core-abstractions)

zijian666 系列组件的基础抽象定义

### 介绍

组件核心库

### 更新日志

[点击查看](UPLOGS.md)

### 项目示例

[点击查看](example/)

### 安装教程

[nuget - zijian666.Core.Abstractions](https://www.nuget.org/packages/zijian666.Core.Abstractions)

### 示例项目

[点击查看](example/)

### 使用说明

#### 1. `FeatureManager` (功能组件)

> 简易版 IoC  
> 主要是用来管理所有 `zijian666` 相关的功能

##### 定义组件

```csharp
class Calc : IFeature{
	public int Add(int a,int b) => a + b;
}
```

##### 使用组件

```csharp

// 直接获取
var calc = FeatureManager.Get<Calc>();
Console.WriteLine(calc.Add(1, 1));


// 手动注入
class Features : ISlot<Calc>
{
    public Calc Calc;
    public void Set(Calc feature) => Calc = feature;
}

var features = new Features();
FeatureManager.Wired(features);
Console.WriteLine(features.Calc.Add(1, 1));

// 组件注入
class Features : ISlot<Calc>, IFeature
{
    public Calc Calc;
    public void Set(Calc feature) => Calc = feature;
}
var features = FeatureManager.Get<Features>();
Console.WriteLine(features.Calc.Add(1, 1));

// 自动刷新
class Features : IAutoRefreshMultiSlot<IFeature>, IFeature
{
    public List<TestFeature> Items { get; private set; }

    public void MultiSet(IEnumerable<IFeature> features)
    {
        Items = features.ToList();
    }
}
var features = FeatureManager.Get<Features>();
Console.WriteLine(features.Items.Count); // 0

FeatureManager.Register(new MyFeature());
Console.WriteLine(features.Items.Count); // 1
```

#### 2. `ErrorReporter`(异常报告)

> 简易版订阅发布应用  
> 只关注异常的

##### 报告异常

```csharp
ErrorReporter.OutChannels = OutChannels.Console | OutChannels.Trace;
try
{
    var i = 0;
    Console.WriteLine(i / i);
}
catch (Exception e)
{
    ErrorReporter.Report(e);
}
```

##### 订阅异常

> 实现`IErrorObserver`会自动订阅上报的异常

```csharp
class ErrorConsole : IErrorObserver
{
    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        Console.WriteLine("error:" + error);
    }

    public void OnNext(ErrorInfo value)
    {
        Console.WriteLine(value.ToString());
    }
}

struct ErrorInfo
{
    /// <summary>
    /// 异常对象
    /// </summary>
    public readonly Exception Exception;
    /// <summary>
    /// 异常产生类
    /// </summary>
    public readonly Type Owner;
    /// <summary>
    /// 上下文参数
    /// </summary>
    public readonly object Context;
    /// <summary>
    /// 异常类成员
    /// </summary>
    public readonly string MemberName;
    /// <summary>
    /// 异常源码文件名
    /// </summary>
    public readonly string FilePath;
    /// <summary>
    /// 异常类源码文件行号
    /// </summary>
    public readonly int LineNumber;
}
```