### [2.0.5] 2024-11-25
+ 新增`IOrdered`接口表示可排序对象
+ `FeatureManager`支持可排序的`IFeature`对象

### [2.0.3] 2023-04-20
+ 针对 `FeatureManager` 提供一个简易的 `IServiceProvider` 实现

### [2.0.2] 2022-11-13
+ `System.Net.Http` 降到4.3.0
+ `System.ValueTuple` 降到4.3.0
+ 降低安装需求

### [2.0.0] 2022-11-05
* 优化代码
* 修复配套组件的部分兼容性问题
* 同时支持.net4.5+

### [1.0.14-beta] 2022-04-15
* 增加`TaskSync`类

### [1.0.13-beta] 2021-09-29
* 修复当一个类型拥有重复特性时,调用`GetTypesWithAttribute`失败的问题

### [1.0.12-beta] 2021-09-28
* 保证更好的兼容性, 依赖基础框架退回到 `netstandard2.0`

### [1.0.8-beta] 2021-09-18
* 修复bug

### [1.0.7-beta] 2021-09-08
+ 增加自动刷新功能插槽的接口`IAutoRefreshSlot`,`IAutoRefreshSlot<T>`和`IAutoRefreshMultiSlot`
+ 优化性能
+ 修复已知问题

### [1.0.6-beta] 2021-08-10
+ 默认加载当前程序路径下的所有dll

### [1.0.5] 2021-03-16
+ 优化`Assembly`的处理逻辑

### [1.0.4] 2020-09-22
+ 增加 `IErrorObserver` 用于处理异常
+ 增加 `ErrorReporter` 用于收集异常, 并传递给 `IErrorObserver` 处理

### [1.0.2] 2020-09-21
+ 修复自动状态遇到特殊类时的bug

### [1.0.0] 2020-09-06
+ 初始版本
+ 字符串序列化器
+ 数据转换器
+ 反射编译器