﻿using System;
using zijian666.Core;

namespace ErrorReporterDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1");
            ReportError();
            Console.WriteLine("===");


            Console.WriteLine("2");
            ErrorReporter.OutChannels = OutChannels.Console | OutChannels.Trace;
            ReportError();
            Console.WriteLine("===");

            Console.WriteLine("3");
            ErrorReporter.OutChannels = OutChannels.None;
            ReportError();
            Console.WriteLine("===");
        }

        static void ReportError()
        {
            try
            {
                var i = 0;
                Console.WriteLine(i / i);
            }
            catch (Exception e)
            {
                ErrorReporter.Report(e);
                throw;
            }
        }
    }
}
