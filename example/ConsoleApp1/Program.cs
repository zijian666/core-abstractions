﻿using System;
using zijian666.Core;
using zijian666.Core.Abstractions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var calc = FeatureManager.Get<Calc>();
            Console.WriteLine(calc.Add(1, 1));


            {
                var features = FeatureManager.Get<Features>();
                Console.WriteLine(features.Calc.Add(1, 1));
            }

            {
                var features = new Features();
                FeatureManager.Wired(features);
                Console.WriteLine(features.Calc.Add(1, 1));
            }

        }



        class Features : ISlot<Calc>, IFeature
        {
            public Calc Calc;
            public void Set(Calc feature) => Calc = feature;
        }
    }
}
