﻿using System;

namespace zijian666.Core.Abstractions
{
    /// <summary>
    /// 字符串序列化器
    /// </summary>
    public interface IStringSerializer : IFeature
    {
        /// <summary>
        /// 序列化协议
        /// </summary>
        string Protocol { get; }
        /// <summary>
        /// 对象转为字符串
        /// </summary>
        /// <param name="value">对象实例</param>
        /// <returns></returns>
        string ToString(object value);
        /// <summary>
        /// 字符串转为泛型对象
        /// </summary>
        /// <param name="value">对象实例</param>
        /// <param name="type">转换类型</param>
        /// <returns></returns>
        object ToObject(string value, Type type);
    }
}
