﻿using System;

namespace zijian666.Core
{
    class Error2Console : IObserver<ErrorInfo>
    {
        public static Error2Console Instance { get; } = new Error2Console();

        public void OnCompleted() { }

        public void OnError(Exception value) => Console.WriteLine($"Error2Console ERROR: {DateTime.Now:yyyy-MM-dd HH:mm:ss}\n{value}");

        public void OnNext(ErrorInfo value) => Console.WriteLine($"ERROR: {DateTime.Now:yyyy-MM-dd HH:mm:ss}\n{value}");
    }
}
