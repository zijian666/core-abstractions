﻿using System;
using System.Diagnostics;

namespace zijian666.Core
{
    class Error2Trace : IObserver<ErrorInfo>
    {
        public static Error2Trace Instance { get; } = new Error2Trace();

        public void OnCompleted() { }

        public void OnError(Exception value) => Trace.Write(value, "Error2Trace ERROR");

        public void OnNext(ErrorInfo value) => Trace.Write(value, "ERROR");
    }
}
