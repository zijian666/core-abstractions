﻿using System;

namespace zijian666.Core.Abstractions
{
    /// <summary>
    /// 异常信息观察者
    /// </summary>
    public interface IErrorObserver : IObserver<ErrorInfo>, IFeature
    {

    }
}
