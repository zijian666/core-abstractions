﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zijian666.Core
{
    /// <summary>
    /// 异常信息输出频道
    /// </summary>
    [Flags]
    public enum OutChannels
    {
        /// <summary>
        /// 无
        /// </summary>
        None = 0b_0000,
        /// <summary>
        /// <see cref="Console"/>
        /// </summary>
        Console = 0b_0001,
        /// <summary>
        /// <see cref="Trace"/>
        /// </summary>
        Trace = 0b_0010,
        /// <summary>
        /// <see cref="DiagnosticSource"/>
        /// </summary>
        DiagnosticSource = 0b_0100,
    }
}
