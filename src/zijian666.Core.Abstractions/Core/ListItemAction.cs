﻿namespace zijian666.Core;

/// <summary>
/// 列表元素动作
/// </summary>
[Flags]
internal enum ListItemAction
{
    /// <summary>
    /// 被添加到集合
    /// </summary>
    Add = 1,
    /// <summary>
    /// 从集合移除
    /// </summary>
    Remove = 2,
    /// <summary>
    /// 替换操作
    /// </summary>
    Replace = 4,
    /// <summary>
    /// 因为替换某个元素而被添加到集合
    /// </summary>
    AddByReplace = Add | Replace,
    /// <summary>
    /// 因为被某个元素替换而从集合中移除
    /// </summary>
    RemoveByReplace = Remove | Replace,
}
