﻿#if NETSTANDARD2_0_OR_GREATER
using System.Reflection;
using System.Runtime.Loader;

namespace zijian666.Core;

public partial class AssemblyCollection
{
    /// <summary>
    /// 加载程序集, 并添加到集合
    /// </summary>
    /// <param name="assemblyName"></param>
    public void LoadFromAssemblyName(AssemblyName assemblyName)
    {
        if (_descriptions.Contains(x => x.AssemblyName.Equals(assemblyName)))
        {
            return;
        }
        _ = Add(AssemblyLoadContext.Default.LoadFromAssemblyName(assemblyName));
    }

    /// <summary>
    /// 按路径加载程序集并添加到集合
    /// </summary>
    /// <param name="assemblyPath"></param>
    public void LoadFromAssemblyPath(string assemblyPath)
    {
        if (_descriptions.Contains(x => x.Location.Equals(assemblyPath, StringComparison.Ordinal)))
        {
            return;
        }
        _ = Add(AssemblyLoadContext.Default.LoadFromAssemblyPath(assemblyPath));
    }
}

#endif