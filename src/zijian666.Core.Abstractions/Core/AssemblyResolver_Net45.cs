﻿#if NET45_OR_GREATER

using zijian666.Extensions;

namespace zijian666.Core;

public partial class AssemblyResolver
{
    /// <summary>
    /// 默认解析器
    /// </summary>
    public static AssemblyResolver Default { get; } = new AssemblyResolver(AppDomain.CurrentDomain.LoadAllAssemblies(), AppDomain.CurrentDomain);

}

#endif