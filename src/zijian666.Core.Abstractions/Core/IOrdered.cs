﻿namespace zijian666.Core.Abstractions;

/// <summary>
/// 可排序对象
/// </summary>
public interface IOrdered
{
    /// <summary>
    /// 优先级
    /// </summary>
    int Order { get; }
}
