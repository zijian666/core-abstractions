using System;

namespace zijian666.Core
{
    internal static class Throws
    {
        public static void NotFoundFeature(Type type)
        {
            var message = $@"缺少功能组件{type.Name}；
请先通过 nuget 安装对应的组件包，
或直接调用 {nameof(FeatureManager)}.{nameof(FeatureManager.Register)} 注册。";
            throw new EntryPointNotFoundException(message);
        }
    }
}
