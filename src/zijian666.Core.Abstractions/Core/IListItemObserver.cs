﻿namespace zijian666.Core.Abstractions;

/// <summary>
/// 表示一个带有回调方法的集合元素
/// </summary>
internal interface IListItemObserver
{
    void Changed(object list, ListItemAction action);
}
