﻿namespace zijian666.Core.Abstractions;

/// <summary>
/// 功能插槽
/// </summary>
/// <typeparam name="Feature"></typeparam>
public interface ISlot<Feature>
{
    /// <summary>
    /// 将功能设置到插槽
    /// </summary>
    void Set(Feature feature);
}

/// <summary>
/// 注册或移除功能时自动刷新指定类型的插槽
/// </summary>

public interface IAutoRefreshSlot<Feature> : ISlot<Feature> { }

/// <summary>
/// 注册或移除功能时自动刷新所有插槽
/// </summary>
public interface IAutoRefreshSlot { }
