﻿#if NETSTANDARD2_0_OR_GREATER

using System.Runtime.Loader;

namespace zijian666.Core;

public partial class AssemblyResolver
{
    /// <summary>
    /// 默认解析器
    /// </summary>
    public static AssemblyResolver Default { get; } =
        // 默认从当前目录下加载所有可被加载的dll程序
        new AssemblyResolver(Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll")?.Select(x =>
        {
            try
            {
                return AssemblyLoadContext.Default.LoadFromAssemblyPath(x);
            }
            catch
            {
                return null;
            }
        }).Where(x => x is not null).Select(x => x!), AppDomain.CurrentDomain);

}

#endif