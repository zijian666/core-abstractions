﻿using System.Collections.Generic;

namespace zijian666.Core.Abstractions
{
    /// <summary>
    /// 功能插槽
    /// </summary>
    /// <typeparam name="Feature"></typeparam>
    public interface IMultiSlot<Feature>
    {
        void MultiSet(IEnumerable<Feature> features);
    }

    /// <summary>
    /// 新增删除注册新组件自动刷新插槽
    /// </summary>

    public interface IAutoRefreshMultiSlot<Feature> : IMultiSlot<Feature>
    {
    }
}
