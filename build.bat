@echo off
chcp 65001
setlocal enabledelayedexpansion

set local="d:\apps\.nuget"

echo 输入数字选择发布位置
echo 1. push %local%
echo 2. push nuget.org
set /p m=请选择:


if "%m%"=="1"  (
    if not exist  %local% (mkdir %local% )
) else (
    if not "%m%"=="2" (
        echo 输入有误, 退出脚本
        pause
        exit 0
    )
)

for /R ".\\" %%f in (*.nupkg) do (del "%%f")

echo ----- 打包项目 ------------------------------------
for /R ".\\src\\" %%f in (*.csproj) do (
    echo 正在打包 ^>^>^> "%%f"
    dotnet build "%%f" --nologo -p:WarningLevel=0 -c Release --force >nul
    dotnet pack "%%f" --nologo -p:WarningLevel=0 -c Release --force
)


echo ----- 打包完成 ------------------------------------
for /R ".\\" %%f in (*.nupkg) do (echo "%%f")
echo ---------------------------------------------------

set /p o=回车继续 Ctrl+c退出

for /R ".\\" %%f in (*.nupkg) do (
    echo ----- 准备推送 %%~nf  ------------------------------
    
    if "%m%"=="1"  (
        dotnet nuget push "%%f" -s "%local%"
    ) else (
        echo "dotnet nuget push "%%f" -s "https://api.nuget.org/v3/index.json" -k %NUGET_KEY% -n -d --skip-duplicate --no-service-endpoint"
        echo ---------------------------------------------------
        set /p o=输入1开始推送:
        if "!o!"=="1"  (
            dotnet nuget push "%%f" -s "https://api.nuget.org/v3/index.json" -k %NUGET_KEY% -n -d --skip-duplicate --no-service-endpoint
            set o=0
        ) else (
            echo 忽略
        ) 
    )
)
for /R ".\\" %%f in (*.nupkg) do (del "%%f")  