﻿using System.Collections.Generic;
using System.Linq;
using zijian666.Core.Abstractions;

namespace UnitTest
{
    class Feature2 : TestFeature, IFeature, ISlot<Feature1>, IAutoRefreshMultiSlot<TestFeature>, IAutoRefreshSlot<Feature3>
    {
        public Feature1 Feature1 { get; private set; }

        public void Set(Feature1 feature)
        {
            Feature1 = feature;
        }

        public List<TestFeature> Features { get; private set; }
        public Feature3 Feature3 { get; private set; }

        public void MultiSet(IEnumerable<TestFeature> features)
        {
            Features = features.ToList();
        }

        public void Set(Feature3 feature)
        {
            Feature3 = feature;
        }
    }
}
