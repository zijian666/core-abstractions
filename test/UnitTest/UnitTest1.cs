﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using zijian666.Core;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var f1 = FeatureManager.Get<Feature1>();
            var f2 = FeatureManager.Get<Feature2>();

            Assert.IsNotNull(f1);
            Assert.IsNotNull(f1.Feature);
            Assert.IsNotNull(f1.Feature2);
            Assert.IsNotNull(f2);
            Assert.IsNotNull(f2.Feature1);
            Assert.IsNull(f1.Feature3);
            Assert.IsNull(f2.Feature3);
            Assert.AreEqual(2, f1.Features.Count);
            Assert.AreEqual(2, f2.Features.Count);

            Assert.AreEqual(f1, f1.Feature);
            Assert.AreEqual(f2.Feature1, f1.Feature);
            Assert.AreEqual(f2, f1.Feature2);

            Assert.IsTrue(f2.Features[0] == f1 || f2.Features[1] == f1);
            Assert.IsTrue(f2.Features[0] == f2 || f2.Features[1] == f2);


            FeatureManager.Register(new Feature3());
            Assert.IsNull(f1.Feature3);
            Assert.IsNotNull(f2.Feature3);
            Assert.AreEqual(2, f1.Features.Count);
            Assert.AreEqual(3, f2.Features.Count);

        }

        [TestMethod]
        public void TestErrorReporter()
        {
            Console.WriteLine("1");
            try
            {
                var i = 0;
                Console.WriteLine(i / i);
            }
            catch (Exception e)
            {
                ErrorReporter.Report(e);
            }
            Console.WriteLine("===");
            Console.WriteLine("2");
            ErrorReporter.OutChannels = OutChannels.Console;
            try
            {
                var i = 0;
                Console.WriteLine(i / i);
            }
            catch (Exception e)
            {
                ErrorReporter.Report(e);
            }

            Console.WriteLine("===");
            Console.WriteLine("3");

            ErrorReporter.OutChannels = OutChannels.None;
            try
            {
                var i = 0;
                Console.WriteLine(i / i);
            }
            catch (Exception e)
            {
                ErrorReporter.Report(e);
            }
            Console.WriteLine("===");
        }
    }
}
