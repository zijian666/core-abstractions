﻿using System.Collections.Generic;
using System.Linq;
using zijian666.Core.Abstractions;

namespace UnitTest
{
    class Feature1 : TestFeature, IFeature, ISlot<Feature1>, ISlot<Feature2>, ISlot<Feature3>, IMultiSlot<TestFeature>
    {
        public Feature1 Feature { get; private set; }
        public Feature2 Feature2 { get; private set; }
        public List<TestFeature> Features { get; private set; }
        public Feature3 Feature3 { get; private set; }

        public void Set(Feature1 feature)
        {
            Feature = feature;
        }

        public void Set(Feature2 feature)
        {
            Feature2 = feature;
        }
        public void MultiSet(IEnumerable<TestFeature> features)
        {
            Features = features.ToList();
        }

        public void Set(Feature3 feature)
        {
            Feature3 = feature;
        }
    }
}
