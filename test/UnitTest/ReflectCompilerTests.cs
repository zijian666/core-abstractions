﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using zijian666.Core;
using zijian666.Core.Abstractions;

namespace UnitTest
{

    [TestClass]
    public class ReflectCompilerTests
    {
        [TestMethod]
        public void MyTestMethod()
        {
            var reflect = FeatureManager.Get<IReflectCompiler>();
            var field = typeof(MyService6Base).GetField(nameof(MyService6Base.str));
            var setValue = reflect.CompileSetter<string>(field);
            var instance = new MyService6();
            setValue(instance, "Hello");
            Assert.AreEqual("Hello", instance.str);
        }




        abstract class MyService6Base
        {
            public readonly string str;
        }

        class MyService6 : MyService6Base
        {

        }

    }
}

